# ancient-genius
In the late 13th century, the renowned mathematician name missing lay on his deathbed. Before his passing, he decided to leave a cryptic message on his grave, an enigmatic sequence of numbers. These numbers appeared to be unrelated, lacking the characteristic pattern.

As time went by, mathematicians and scholars puzzled over these seemingly random numbers, attempting to decipher their meaning. It became a mathematical mystery, a challenge to uncover the hidden message left by the brilliant mind of a mysterious person. Despite numerous attempts, the code remained unbroken.

To this day, the numbers on the grave of this mysterious person continue to perplex and intrigue those who come across them, a testament to the enduring legacy of a mathematical genius who left a final puzzle for the world to unravel.

The photograph of the grave:
<div style="text-align:center"><img src="grave.png" width="70%" /></div>

## solving steps
0. the spiral on the back is clearly a [fibonacci spyral](https://en.wikipedia.org/wiki/Fibonacci_sequence)

    ![spyral](spyral.png)

    so i supposed that the numbers on the grave were numbers of the fibonacci sequence

0. Therefore i created the function `is_fibonacci(n)` in order to check if those were actually fibonacci numbers
0. To extract the numbers from the image i used an OCR software, and later did some manual corrections
0. after checking if all were fibonacci numbers (and they were), i realized that on the back of the image there is a forum, which reminded me of something from the roman empire. So i instantly thought about the [ceasar cypher](https://en.wikipedia.org/wiki/Caesar_cipher).
The immediate reaction was implementing a simple caesar cypher with n=0 and using the ordinal number of the fibonacci sequence as glyphs (eg. 5 has ordinal fibonacci value of 4). this got to the result "ohmfzsdrsnv`^ek`f`|"
    > after further research, it looked a bit like the [forum of ceasar](https://en.wikipedia.org/wiki/Forum_of_Caesar)
0. the flags are formatted as `ping{.*}` and `p` has ASCII distance 1 to `o`, this means that $n=1$. adding this offset to the algorithm got me to the flag (`ping{testowa_flaga}`) 🥳 
